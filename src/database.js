const seq = require('sequelize');


const sequelize = new seq("dbfortasks","tejas22","tejas@22",{
    host:"db4free.net",
    dialect: "mysql"
});

const SeqConnection = async () => {
    try {
        await sequelize.authenticate()
        console.info("Database connection enabled successfully.")
    } catch (error) {
        console.error(`ERROR: ${error.message} while connecting.`)
    }
}

SeqConnection()

module.exports = sequelize;
