const jwt = require('jsonwebtoken')


const authMiddleware = (req,res,next) => {
    
        let jwtToken = null
        const authHeader = req.headers["authorization"]
        if (authHeader !== undefined) {
            jwtToken = authHeader.split(" ")[1]
            // console.log(jwtToken);
        }

        if (jwtToken !== undefined) {
            jwt.verify(jwtToken,process.env.SECRET_KEY, (error,payload) => {

                if (error) {
                    res.status(403).send("Invalid Access Token.")
                    console.error("Invalid Access Token.")
                } 
                else {
                    req.email = payload.email
                    next()
                }
            })
        } 
        else {
            res.status(404).send("JWT Token in not found")
        }
    

}

module.exports = authMiddleware;

