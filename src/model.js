const sequelize = require('./database')
const {Sequelize, DataTypes} = require('sequelize')

const user = sequelize.define('user',{
    id: {
        primaryKey: true,
        allowNull: false,
        autoIncrement: true,
        type:DataTypes.INTEGER
    },
    name: {
        type: DataTypes.STRING,
        length: 255,
        allowNull: false
    },
    email: {
        type: DataTypes.STRING,
        allowNull: null,
        unique: true,
        length: 255,
    },
    password: {
        type: DataTypes.STRING,
        length: 255,
        allowNull: false
    }
},{
    timestamps: false
});

const task  = sequelize.define('task', {
    task_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        allowNull: false,
        autoIncrement: true
    },
    title: {
        type: DataTypes.STRING,
        length: 255,
        allowNull: false
    }
},{
    timestamps: false
});

const subtask = sequelize.define('subtask', {
    subtask_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true,
        primaryKey: true
    },
    title: {
        type: DataTypes.STRING,
        length: 255,
        allowNull: false
    }
},{
    timestamps: false
});

const refreshtoken = sequelize.define('refreshtoken',{
    id: {
        primaryKey: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        autoIncrement: true
    },
    email: {
        type: DataTypes.STRING,
        length: 255,
        allowNull: false,
        unique: true
    },
    token: {
        type: DataTypes.STRING,
        length: 255,
        allowNull: false,
        unique: true
    }
})

user.hasMany(task);
task.belongsTo(user);

task.hasOne(subtask);
subtask.belongsTo(task)

const creatingInstanceToModel = async () => {

    await task.create({
        title: "Add the new tables in the database.",
        userId: 2
    });

    await subtask.create({
        title: "Update the data into the new tables in database.",
        taskTaskId: 2
    })
};

sequelize.sync() 

module.exports = {user,task,subtask,refreshtoken}
