const express = require('express')
const router = express.Router()
const bcrypt = require('bcrypt')
const jwt = require('jsonwebtoken')
const dotenv = require('dotenv')
const sequelize = require('./src/database')
const authMiddleware = require('./src/middleware')
dotenv.config()

const isNameValid = (name) => {
    const regex = /^[a-zA-Z ]{5,20}$/;
    return regex.test(name)
}

const isEmailValid = (email) => {
    const regex = /^[a-z0-9]+@[a-z]+\.[a-z]{2,3}$/;
    return regex.test(email)
}

const isPassValid = (password) => {
    return  ((password.length >= 5) && (password.length <= 200)) 
}

const isContentValid = (content) => {
    const regex = /^[a-zA-Z ]{5,300}$/;
    return regex.test(content)
}


//For Signup
const {user, task, subtask,refreshtoken} = require('./src/model')
router.post('/signup', async (req,res) => {

        const name = req.body.name
        const email = req.body.email
        const password = req.body.password

        
        if (!isNameValid(req.body.name)) {
            res.status(400).send(`Invalid name. Name contains only alphabets.`)

        } else if (!isEmailValid(req.body.email)){
            res.status(400).send(`${req.body.email} : Invalid Email`)

        } else if (!isPassValid(req.body.password)) {
            res.status(400).send(`Characters in the password must be lies between 6 to 256`)

        } else {
            const hashedPassword = await bcrypt.hash(password,10)
            const isUserFound = await user.findOne({
                where: {email :  req.body.email}
            });

            if (isUserFound) {
                res.status(400).send(`User with Email: ${email} is already exists in the database.`)

            } else {
                const userData = await user.create({
                name,
                email,
                password: hashedPassword
            })
            res.status(200).send(userData)
            }
        }
        
    
});


//Login 

router.post('/login',async (req,res) => {
  
        const isUserFound = await user.findOne({
            where: {email :  req.body.email}
        });

        if (isUserFound) {
            const isPasswordMatched = await bcrypt.compare(req.body.password,isUserFound.password)
            console.log(`is matched ${isPasswordMatched}`)

            if (isPasswordMatched) {
                const payload = {
                    email: req.body.email
                }

                const jwtToken = generateAccessToken(payload)
                const refreshToken = jwt.sign(payload, process.env.REFRESH_KEY)
                await refreshtoken.create({
                    email: req.body.email,
                    token: refreshToken
                })

                res.status(200).json({jwtToken,refreshToken})

            } else{
                res.status(401).send(`Password is incorrect.`)
            }

        } else {
            res.status(404).send(`User with EMAIL: ${req.body.email} is not found.`)
        }
   
})

function generateAccessToken(payload) { 

    return jwt.sign(payload, process.env.SECRET_KEY,{expiresIn:"45s"})
}

// Token

router.post('/token', async (req,res)=>{

        if (req.body.token === null) {
            console.error("Unauthorized User")
            res.status(401).send("Unauthorized user")
        } 
        else {
            const isTokenFound = await refreshtoken.findOne({
                where: {token: req.body.token}
            })
            if (isTokenFound) {
                jwt.verify(req.body.token,process.env.REFRESH_KEY, (error,payload)=>{
                    if (error) {
                        return res.status(403).send("Invalid Refresh Token")
                    }
                    const accessToken = generateAccessToken({email:payload.email})
                    console.info("New Access Token Generated.")
                    res.send({accessToken})
                })
            } 
            else {
                res.status(404).send(`Refresh token is not found`)
            }
        }
    
});

// Read Users

router.get('/users',authMiddleware, async (req,res) => {
        const usersData = await user.findAll()
        res.status(200).send(usersData)
   
});

//Read Task

router.get('/users/task/:id',authMiddleware, async (req,res) => {

        const taskData = await task.findOne({
            where:{ 
                task_id: req.params.id
            }
        })
        if (taskData) {
            res.status(200).send(taskData)
        } else {
            res.status(404).send(`Task with task_id: ${req.params.id} is not found.`)
        }
    
});

//Read Subtasks

router.get('/users/subtask/:id',authMiddleware, async (req,res) => {
    
        const subtaskData = await subtask.findOne({
            where:{
                subtask_id: req.params.id
            }
        })
        if (subtaskData) {
            res.status(200).send(subtaskData)
        } else {
            res.status(404).send(`Subtask with subtask_id: ${req.params.id} is not found.`)
        }
});


//Read All Tasks

router.get('/tasks',authMiddleware, async (req,res) => {
    
        const tasksData = await task.findAll()
        if (subtaskData) {
            res.status(200).send(tasksData)
        } else {
            res.status(404).send(`Task data did not found.`)
        }
        
   
});

// Read Subtasks

router.get('/subtasks',authMiddleware, async (req,res) => {
    
        const subtasksData = await subtask.findAll()
        if (subtasksData){
        res.status(200).send(subtasksData)
        } 
        else {
        res.status(500).send(error.message)
        }
});

//Update Users Info

router.put('/users/:id', authMiddleware, async (req,res) => {

        const isUserFound = await user.findOne({
            where: {
                id: req.params.id
            }
        });
        if (isUserFound) {
            await user.update({name: req.body.name,email:req.body.email}, {
                where: {
                    id: req.params.id
                }
            })
            res.status(200).send(`User with ID: ${req.params.id} is updated sucessfully`)
        } 
        else {
            res.status(404).send(`User with ID: ${req.params.id} is not found in the database.`)
        }
   
})

//Update Task.

router.put('/users/tasks/:id', authMiddleware, async (req,res) => {
    
        const isTaskFound = await task.findOne({
            where: {
                task_id: req.params.id
            }
        });
        if (isTaskFound) {
            await task.update({title: req.body.title,userId:req.body.userId}, {
                where: {
                    task_id: req.params.id
                }
            })
            res.status(200).send(`Task with ID: ${req.params.id} is updated sucessfully`)
        } 
        else {
            res.status(404).send(`Task with ID: ${req.params.id} is not found in the database.`)
        }
})

//Update Subtask

router.put('/users/subtasks/:id', authMiddleware, async (req,res) => {
    
        const isTaskFound = await subtask.findOne({
            where: {
                subtask_id: req.params.id
            }
        });
        if (isTaskFound) {
            await subtask.update({title: req.body.title,taskTaskId:req.body.taskTaskId}, {
                where: {
                    subtask_id: req.params.id
                }
            })
            res.status(200).send(`Subtask with ID: ${req.params.id} is updated sucessfully`)
        } else {
            res.status(404).send(`Subtask with ID: ${req.params.id} is not found in the database.`)
        }
    
})

// Delete User

router.delete('/users/delete/:id', authMiddleware, async (req,res) => {
    
        const isUserFound = await user.findOne({
            where: {
                id: req.params.id
            }
        });
        if (isUserFound) {
            await user.update({name: req.body.name,email:req.body.email}, {
                where: {
                    id: req.params.id
                }
            })
            res.status(200).send(`User with ID: ${req.params.id} is deleted sucessfully`)
        } 
        else {
            res.status(404).send(`User with ID: ${req.params.id} is not found in the database.`)
        }
});

//Add Task

router.post('/tasks',authMiddleware, async (req,res) => {
    
        if (!isContentValid(req.body.title)) {
            res.status(400).send(`Characters in the title lies between 20 to 255`)
        }
        
        else {
            const newTask = await task.create({
                title :  req.body.title,
                userId : req.body.userId
            });

            res.status(200).send(newTask)
        }
});


// Add Subtask
router.post('/subtasks',authMiddleware, async (req,res) => {
    
        if (!isContentValid(req.body.title)) {
            res.status(400).send(`Characters in the title lies between 20 to 255`)
        }
        else {
            const newSubTask = await subtask.create({
                title :  req.body.title,
                taskTaskId : req.body.taskId
            });

            res.status(200).send(newSubTask)
        }
    
});

module.exports = router;
