const express = require('express');

const app = express()
app.use(express.json())
app.use(express.urlencoded({extended:true}))

app.use('/api', require('./router'))

const PORT = process.env.PORT || 3000

app.listen(PORT, () => console.info(`Server Started listening on the http://localhost:${PORT}`))


